FROM microsoft/aspnetcore-build:latest
RUN apt-get -y update && \
	curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -  && \
	curl -sL https://deb.nodesource.com/setup_6.x | bash - && \
	curl -o- -L https://yarnpkg.com/install.sh | bash && \
	export PATH=$HOME/.yarn/bin:$PATH && \
	curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar -zx && \
	mv cf /usr/local/bin && \
    apt-get -y install \
        nodejs \
        git  && \
    apt-get clean && \
    npm set progress=false && \
    npm config set scope @cemex && \
    npm i -g cypress-cli && \
    cypress install && \
    npm cache clean